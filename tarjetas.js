
const obj_1 = {
    nombre: "img2",
    urlImagen: "https://s2.coinmarketcap.com/static/img/coins/200x200/11130.png"
};

const obj_2 = {
    nombre: "img3",
    urlImagen: "https://ganaconjuegosnft.com/wp-content/uploads/2021/09/4_1.png"
}

const obj_3 = {
    nombre: "img4",
    urlImagen: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSdGGz8Jl-N996GGY80xXpflDMsXRrAVuyOvg&usqp=CAU"
}

const obj_4 = {
    nombre: "img5",
    urlImagen: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSMbZx9Q_07HTfY74rwa_fFffaJ-WjlBRnwTw&usqp=CAU"
}

const objetos = [obj_1, obj_2, obj_3, obj_4];

/**********************************
 * Ejercicio:
 * 
 * Cargar en el DOM los objetos del arreglo en cards
 * 
 **********************************/

function cargarTarjetas() {
    let contenedor = document.getElementById('contenedor');
    //iterar el arreglo
    objetos.forEach(element => {
        console.log(element);
        let tarjeta = "<div class='card' style='width: 18rem;'>"
        tarjeta += "<img id = 'imagen' alt = '"+element.nombre+"'"
        tarjeta += "src = '"+element.urlImagen+"' class='card-img-top'>"
        tarjeta +=  " <div class='card-body'>"
        tarjeta += "<h5 class='card-title'>"+element.nombre+"</h5>"
        tarjeta += "</div> </div> "
        //añadir html al contenedor
        contenedor.innerHTML += tarjeta;
    });
}