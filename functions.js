
function saludar() {
    console.log("Hola mundo");
    console.error("Hola mundo desde un error");
    console.warn("Hola mundo desde un warning");
}

/*
function sumar() {
    //Obtener un elemento html por id
    let num1 = document.getElementById('num1').value;
    let num2 = document.getElementById('num2').value;
    //sumar
    let resultado = parseFloat(num1) + parseFloat(num2);
    console.log("La suma es: ", resultado);
    //alert("El resultado es: "+resultado);
    document.getElementById('resultado').innerHTML = "" + resultado;
}
*/

/***************************************************************
 * Ejercicio:
 * Desarrolle las funcionalidades restar, multiplicar, dividir
 * limpie los campos de texto cada vez que ejecute una operación.
 ****************************************************************/

//----------SOLUCIÓN DE CESAR CAMILO---------


function obtNumeros() {
    let num1 = document.getElementById('num1').value;
    let num2 = document.getElementById('num2').value;
    return { num1, num2 }
}

function limpiar() {
    document.getElementById('num1').value = '';
    document.getElementById('num2').value = '';
}

function sumar() {
    //obtener un elemento html por id
    let n = obtNumeros();
    let resultado = parseFloat(n.num1) + parseFloat(n.num2);
    console.log(resultado);
    document.getElementById('resultado').innerHTML = "" + resultado;
    //alert("El resultado es: " +resultado)
    limpiar();
}

function restar() {
    let n = obtNumeros();
    let resultado = parseFloat(n.num1) - parseFloat(n.num2);
    console.log(resultado);
    document.getElementById('resultado').innerHTML = "" + resultado;
}

function multiplicar() {
    let n = obtNumeros();
    let resultado = parseFloat(n.num1) * parseFloat(n.num2);
    console.log(resultado);
    document.getElementById('resultado').innerHTML = "" + resultado;
    limpiar();
}

function dividir() {
    let n = obtNumeros();
    if (parseFloat(n.num2) != 0) {
        let resultado = parseFloat(n.num1) / parseFloat(n.num2);
        console.log(resultado);
        document.getElementById('resultado').innerHTML = "" + resultado;
        limpiar();
    } else {
        alert("No se puede dividir por cero");
    }
}



//Funciones de la tarjeta
function actualizarImagen(){
    console.log("actualizando...");
    let tarjeta = document.getElementById('imagen');
    if(tarjeta.alt == 'img1'){
        tarjeta.src = "https://s2.coinmarketcap.com/static/img/coins/200x200/11130.png";
        tarjeta.alt = "img2";
    }else{
        tarjeta.src= "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRc9krB7_PGvkgzqKVp4Xwy68HZ8EjbVR4GUw&usqp=CAU";
        tarjeta.alt = "img1";
    }
}